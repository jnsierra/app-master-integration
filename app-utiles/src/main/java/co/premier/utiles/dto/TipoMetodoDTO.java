package co.premier.utiles.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TipoMetodoDTO extends AuditoriaDto {

	@ApiModelProperty(notes = "Id de la Auditoria")
	private Long id;
	@ApiModelProperty(notes = "Metodo de la Auditoria")
	private String metodo;
	@ApiModelProperty(notes = "Dias que durara la cache")
	private Long dias_cache;
	@ApiModelProperty(notes = "Codigo unico para identificar el Metodo")
	private String codigo;
	@ApiModelProperty(notes = "Objeto cache")
	private List<AppCacheDTO> cache;

}
