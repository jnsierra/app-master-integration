package co.premier.utiles.dto;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppCacheDTO extends AuditoriaDto {
	@ApiModelProperty(notes = "Fecha en la que inicia la vigencia del cache")
	private Date startDate;
	@ApiModelProperty(notes = "Fecha en la que caducara la cache")
	private Date endDate;
	@ApiModelProperty(notes = "Objeto JSON que se envia como parametro al servicio")
	private String objectIn;
	@ApiModelProperty(notes = "Objeto JSON que se retorna el servicio")
	private String objectOut;
	@ApiModelProperty(notes = "Primer Validador para obtener la cache")
	private String primer_validador;
	@ApiModelProperty(notes = "Segundo Validador para obtener la cache")
	private String segundo_validador;
	@ApiModelProperty(notes = "Lista de servicios que estan relacionados con la cache")
	private List<TipoMetodoDTO> metodos;
	@ApiModelProperty(notes = "Dias de la cache")
	private Long dias;

}
