package co.premier.bussines.service;

import java.util.List;
import java.util.Optional;

import co.premier.repository.entity.TipoMetodoEntity;

public interface ITipoMetodoService {

	public TipoMetodoEntity get(Long id);

	public List<TipoMetodoEntity> getAll();

	public TipoMetodoEntity post(TipoMetodoEntity entity);

	public Optional<TipoMetodoEntity> put(TipoMetodoEntity entity);

	public Boolean delete(Long id);

	public TipoMetodoEntity consultarPorCodigo(String codigo);

}
