package co.premier.bussines.service;

import java.util.List;
import java.util.Optional;

import co.premier.repository.entity.AppCacheEntity;

public interface IAppCacheService {

	/**
	 * Busca la cache por Id
	 * 
	 * @param id Id de la autenticación
	 * @return
	 */
	public AppCacheEntity get(Long id);

	/**
	 * Busca todas caches posibles para una aplicacion
	 * 
	 * @return
	 */
	public Optional<List<AppCacheEntity>> getAll();

	/**
	 * Realiza la insercion de una cache
	 * 
	 * @param entity
	 */
	public AppCacheEntity insert(AppCacheEntity entity);

	/**
	 * Borra la cache
	 * 
	 * @param id
	 * @return
	 */
	public Boolean delete(Long id);

	/**
	 * Consulta cache
	 * 
	 * @param String primerValidador,String segundoValidador,String nombreServicio
	 * @return AppCacheEntity
	 */
	public AppCacheEntity consultarCache(String primerValidador,String segundoValidador,String nombreServicio);
}
