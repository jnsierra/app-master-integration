package co.premier.bussines.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.premier.bussines.service.IAppCacheService;
import co.premier.repository.IAppCacheRepository;
import co.premier.repository.entity.AppCacheEntity;

@Service
public class ImplAppCacheService implements IAppCacheService {

	@Autowired
	IAppCacheRepository appCacheRepository;

	@Override
	public AppCacheEntity get(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<List<AppCacheEntity>> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AppCacheEntity insert(AppCacheEntity entity) {
		return appCacheRepository.save(entity);
	}

	@Override
	public Boolean delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Metodo que se encarga de consultar si existe o no una cache en la tabla
	 * app_cache utilizando los siguientes parametros
	 * 
	 * @param primerValidador: Primer parametro para realizar la consulta.
	 * @param segundoValidador: Segundo parametro para realizar la consulta.
	 * @param nombreServicio: Tercer parametro para realizar la consulta, este
	 *        parametro debe ser el nombre del metodo que tiene parametrizad el
	 *        servicio en la base de datos.
	 */
	@Override
	public AppCacheEntity consultarCache(String primerValidador, String segundoValidador, String nombreServicio) {
		return appCacheRepository.consultarCache(primerValidador, segundoValidador, nombreServicio);
	}

}
