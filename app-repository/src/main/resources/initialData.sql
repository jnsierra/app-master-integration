INSERT INTO integracion.app_roles
(id_ro, created_by, created_date, estado, last_modified_by, last_modified_date, nombre)
VALUES(0, 'bootstraping', now(), 'ACTIVO', 'bootstraping', now(), 'ADMIN_ROLE');


INSERT INTO integracion.app_admin
(id_app, created_by, created_date, estado, last_modified_by, last_modified_date, codigo, nombre, numero_consumos)
VALUES(0, 'bootstraping', now(), 'ACTIVO', 'bootstraping', now(), '001', 'EXPERIAN', 0);


INSERT INTO integracion.app_usuarios
(id_us, created_by, created_date, estado, last_modified_by, last_modified_date, codigo, contrasena, nombre, usuario, app_admin_fk)
VALUES(0, 'bootstraping', now(), 'ACTIVO', 'bootstraping', now(), '001','1234', 'jesus nicolas', 'jnsierra', 0);


INSERT INTO integracion.usuario_as_roles
(id_us, id_ro)
VALUES(0, 0);
