package co.premier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.premier.repository.entity.AppCacheEntity;

@Repository
public interface IAppCacheRepository extends CrudRepository<AppCacheEntity, Long>, JpaRepository<AppCacheEntity, Long> {

	AppCacheEntity consultarCache(@Param("primerValidador")String primerValidador,@Param("segundoValidador") String segundoValidador,@Param("nombre") String nombreServicio);

}
