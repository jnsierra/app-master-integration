package co.premier.repository.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import co.premier.repository.enumeracion.MetodoTipoInf;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "app_tipo_metodo")
@NamedQueries({
		@NamedQuery(name = "TipoMetodoEntity.consultarPorCodigo", query = "select a from TipoMetodoEntity a where a.codigo = :codigo") })

public class TipoMetodoEntity extends Auditoria<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_met")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "metodo")
	private MetodoTipoInf metodo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_servicioApli")
	private ServicioEntity servicio;

	@OneToMany(mappedBy = "metodo", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	private List<AppCacheEntity> caches;

	@Column(name = "dias_cache")
	private Long dias_cache;

	@Column(name = "codigo", unique = true)
	private String codigo;

}
