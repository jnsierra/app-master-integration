package co.premier.repository.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@NamedQueries({ @NamedQuery(name = "AppCacheEntity.consultarCache", query = "select a from AppCacheEntity a "
		+ "where a.primer_validador = :primerValidador " + "and  a.segundo_validador = :segundoValidador "
		+ "and  a.metodo.servicio.nombre = :nombre and end_date > sysdate()") })
@Entity
@Table(name = "app_cache")
@EntityListeners(AuditingEntityListener.class)
public class AppCacheEntity extends Auditoria<String> {

	@Id
	@Column(name = "id_cache")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cache_generator")
	@SequenceGenerator(name = "cache_generator", sequenceName = "cache_seq", allocationSize = 1)
	private Long id;

	@Column(name = "object_in", length = 5000)
	private String objectIn;

	@Column(name = "object_out", length = 5000)
	private String objectOut;

	@CreatedDate
	@Column(name = "start_date", updatable = false)
	private Date start_date;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "primer_validador")
	private String primer_validador;

	@Column(name = "segundo_validador")
	private String segundo_validador;

	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "id_met", nullable = false)
	private TipoMetodoEntity metodo;

}
