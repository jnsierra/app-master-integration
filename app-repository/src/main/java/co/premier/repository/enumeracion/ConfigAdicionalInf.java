package co.premier.repository.enumeracion;


public enum ConfigAdicionalInf {
	WSHANDLER_USER,
	WSHANDLER_SIG_PROP_FILE,
	WSHANDLER_SIGNATURE_USER,
	WSHANDLER_MUST_UNDERSTAND,
	WSHANDLER_SIGNATURE_PARTS,
	WSHANDLER_SIG_KEY_ID,
	WSHANDLER_SIG_ALGO,
}
