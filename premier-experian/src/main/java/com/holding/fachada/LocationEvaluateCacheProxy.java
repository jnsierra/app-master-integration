package com.holding.fachada;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.holding.service.IAppCacheService;
import com.holding.service.ILocationService;
import com.parameter.dto.AppCacheDTO;

import co.com.datacredito.services.schema.serviciolocalizacion.v1.ConsultarDatosLocalizacion;

@Service(value = "LocationEvaluateCacheProxy")
public class LocationEvaluateCacheProxy implements ILocationService {

	@Autowired
	@Qualifier("LocationService")
	ILocationService locationService;

	@Autowired
	IAppCacheService appCacheService;

	@Override
	public String validateCacheProxy(ConsultarDatosLocalizacion _consultarDatosLocalizacion_parameters) {
		AppCacheDTO appCacheDTO = appCacheService.existsCache(
				_consultarDatosLocalizacion_parameters.getSolicitud().getSolicitudDatosLocalizacion()
						.getTipoIdentificacion(),
				_consultarDatosLocalizacion_parameters.getSolicitud().getSolicitudDatosLocalizacion()
						.getIdentificacion(),
				"getLocation");
		if (appCacheDTO != null) {
			return appCacheDTO.getObjectOut();
		}
		return locationService.getLocation(_consultarDatosLocalizacion_parameters);
	}

	@Override
	public String saveCacheProxy(ConsultarDatosLocalizacion _consultarDatosLocalizacion_parameters,
			String objectJsonOut, String codeMethod) {
		return null;
	}

	@Override
	public String getLocation(ConsultarDatosLocalizacion _consultarDatosLocalizacion_parameters) {
		return null;
	}

}
