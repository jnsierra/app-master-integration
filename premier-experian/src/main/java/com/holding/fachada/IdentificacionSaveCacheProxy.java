package com.holding.fachada;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.holding.dto.DatosValidacionRequestDto;
import com.holding.dto.RespuestasRequestDto;
import com.holding.dto.SolicitudCuestionarioRequestDto;
import com.holding.service.IFacadeIdentiService;
import com.holding.service.impl.AppCacheService;

@Service(value = "IdentificacionSaveCacheProxy")
public class IdentificacionSaveCacheProxy implements IFacadeIdentiService {

	@Autowired
	AppCacheService appCacheService;
	private Gson gson = new Gson();

	@Override
	public Optional<String> saveCacheProxy(DatosValidacionRequestDto datosValidar, String objectJsonOut,
			String codeMethod) {
		if (200 != appCacheService
				.insertCache(datosValidar.getIdentificacion().getTipo(), datosValidar.getIdentificacion().getNumero(),
						gson.toJson(datosValidar), objectJsonOut, appCacheService.getMethodByCode(codeMethod))
				.value()) {
			return null;
		}
		return Optional.of(objectJsonOut);
	}

	@Override
	public Optional<String> getValidateIdentity(DatosValidacionRequestDto datosValidacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<String> getQuestions(SolicitudCuestionarioRequestDto solicitudCuestionarioRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<String> getVerify(RespuestasRequestDto respuestas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<String> validateCacheProxy(DatosValidacionRequestDto datosValidar) {
		// TODO Auto-generated method stub
		return null;
	}

}
