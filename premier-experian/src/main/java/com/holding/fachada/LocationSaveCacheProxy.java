package com.holding.fachada;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.holding.service.ILocationService;
import com.holding.service.impl.AppCacheService;
import com.parameter.dto.TipoMetodoDTO;

import co.com.datacredito.services.schema.serviciolocalizacion.v1.ConsultarDatosLocalizacion;

@Service(value = "LocationSaveCacheProxy")
public class LocationSaveCacheProxy implements ILocationService {

	@Autowired
	AppCacheService appCacheService;

	private Gson gson = new Gson();

	@Override
	public String saveCacheProxy(ConsultarDatosLocalizacion _consultarDatosLocalizacion_parameters,
			String objectJsonOut, String codeMethod) {

		TipoMetodoDTO tipoMetodoDTO = appCacheService.getMethodByCode(codeMethod);

		if (200 != appCacheService.insertCache(
				_consultarDatosLocalizacion_parameters.getSolicitud().getSolicitudDatosLocalizacion()
						.getTipoIdentificacion(),
				_consultarDatosLocalizacion_parameters.getSolicitud().getSolicitudDatosLocalizacion()
						.getIdentificacion(),
				gson.toJson(_consultarDatosLocalizacion_parameters), objectJsonOut, tipoMetodoDTO).value()) {
			return null;
		}
		
		
		return objectJsonOut;
	}

	@Override
	public String getLocation(ConsultarDatosLocalizacion _consultarDatosLocalizacion_parameters) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String validateCacheProxy(ConsultarDatosLocalizacion _consultarDatosLocalizacion_parameters) {
		// TODO Auto-generated method stub
		return null;
	}

}
