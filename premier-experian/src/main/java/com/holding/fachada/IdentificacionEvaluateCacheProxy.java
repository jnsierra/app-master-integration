package com.holding.fachada;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.holding.dto.DatosValidacionRequestDto;
import com.holding.dto.RespuestasRequestDto;
import com.holding.dto.SolicitudCuestionarioRequestDto;
import com.holding.service.IAppCacheService;
import com.holding.service.IFacadeIdentiService;
import com.parameter.dto.AppCacheDTO;

@Service(value = "IdentificacionEvaluateCacheProxy")
public class IdentificacionEvaluateCacheProxy implements IFacadeIdentiService {

	@Autowired
	@Qualifier("FacadeIdentiService")
	IFacadeIdentiService facadeIdentiService;

	@Autowired
	IAppCacheService appCacheService;

	@Override
	public Optional<String> validateCacheProxy(DatosValidacionRequestDto datosValidar) {
		AppCacheDTO appCacheDTO = appCacheService.existsCache(datosValidar.getIdentificacion().getTipo(),
				datosValidar.getIdentificacion().getNumero(), "getValidarIdentidad");
		if (appCacheDTO != null) {
			return Optional.of(appCacheDTO.getObjectOut());
		}

		return facadeIdentiService.getValidateIdentity(datosValidar);
	}

	@Override
	public Optional<String> getValidateIdentity(DatosValidacionRequestDto datosValidacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<String> getQuestions(SolicitudCuestionarioRequestDto solicitudCuestionarioRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<String> getVerify(RespuestasRequestDto respuestas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<String> saveCacheProxy(DatosValidacionRequestDto datosValidar, String objectJsonOut,
			String codeMethod) {
		// TODO Auto-generated method stub
		return null;
	}

}
