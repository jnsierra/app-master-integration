package com.holding.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.holding.service.IParameterService;
import com.holding.service.IUsuarioService;
import com.parameter.dto.AplicacionIntegraDTO;
import com.parameter.dto.ConfigAdicionalDTO;

@Service
public class ParameterService implements IParameterService{
	
	  @Autowired
	  private IUsuarioService usuarioService;
	  
	  
	  public AplicacionIntegraDTO parameters() {
			
		String token  = usuarioService.token("001");		
	    RestTemplate plantilla = new RestTemplate();	    
	    HttpHeaders authorization = new HttpHeaders();
	    
	    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
	            .queryParam("codigo", "Premier");
	    
	    authorization.add("Authorization","Bearer "+token);
	    authorization.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    
	    HttpEntity<String> entity = new HttpEntity<String>("parameters", authorization);	    
	    
	    String responseBody = plantilla.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class).getBody(); 
	    responseBody =  responseBody.substring(10,responseBody.length()-10);
	    Gson gson = new GsonBuilder().create();
	    AplicacionIntegraDTO app = gson.fromJson(responseBody,AplicacionIntegraDTO.class);
	    
	    
	   return app;
	    
	  }
	  	
	    public String  filtro(String clave,List<ConfigAdicionalDTO> param) {
	    	String valor = null;   	
	    	
	    	for (ConfigAdicionalDTO conFig : param) {
	    		  if (conFig.getConfiguracion().equals(clave)) {
	    			 valor = conFig.getValor();	    			 
	    		  }
	    		}
	    	
	    	return valor;
	         
	    }
}
