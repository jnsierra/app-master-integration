package com.holding.service.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.holding.abs.UtilAbs;
import com.holding.service.IAppCacheService;
import com.parameter.dto.AppCacheDTO;
import com.parameter.dto.TipoMetodoDTO;

@Service
public class AppCacheService extends UtilAbs implements IAppCacheService {

	private HttpHeaders httpHeaders = new HttpHeaders();
	private TipoMetodoDTO tipoMetodoDTO = new TipoMetodoDTO();
	private AppCacheDTO appCacheDTO = new AppCacheDTO();
	private Gson gson = new Gson();

	@Override
	public HttpStatus insertCache(String primerValidador, String segundoValidador, String objectJsonIn,
			String objectJsonOut, TipoMetodoDTO metodoDTO) {
		ResponseEntity<String> result = null;
		try {
			buildCache(primerValidador, segundoValidador, objectJsonIn, objectJsonOut, metodoDTO);
			httpHeaders.set("Authorization", "Bearer ".concat(getToken()));
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> request = new HttpEntity<>(gson.toJson(tipoMetodoDTO), httpHeaders);
			result = new RestTemplate().postForEntity(new URI(urlSaveCache), request, String.class);
			System.out.println("Status: " + result.getStatusCode());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return result.getStatusCode();
	}

	@Override
	public AppCacheDTO existsCache(String primerValidador, String segundoValidador, String metodo) {
		ResponseEntity<String> result = null;
		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlExistsCache)
					.queryParam("primerValidador", primerValidador).queryParam("segundoValidador", segundoValidador)
					.queryParam("nombreServicio", metodo);

			httpHeaders.set("Authorization", "Bearer ".concat(getToken()));
			httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			HttpEntity<String> entity = new HttpEntity<String>("parameters", httpHeaders);
			result = new RestTemplate().exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);
		} catch (HttpServerErrorException e) {
			e.printStackTrace();
			return null;
		}
		return new GsonBuilder().create().fromJson(result.getBody().substring(10, result.getBody().length() - 10),
				AppCacheDTO.class);
	}

	@Override
	public TipoMetodoDTO getMethodByCode(String code) {
		ResponseEntity<String> result = null;
		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlMethodByCode).queryParam("codigo", code);
			httpHeaders.set("Authorization", "Bearer ".concat(getToken()));
			httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<String> entity = new HttpEntity<String>("parameters", httpHeaders);
			result = new RestTemplate().exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);
		} catch (HttpServerErrorException e) {
			return null;
		}
		return new GsonBuilder().create().fromJson(result.getBody().substring(10, result.getBody().length() - 10),
				TipoMetodoDTO.class);
	}

	/**
	 * Metodo que se encarga de construir el objeto de Cache
	 * 
	 * @param objeto
	 */
	public void buildCache(String primerValidador, String segundoValidador, String objectJsonIn, String objectJsonOut,
			TipoMetodoDTO metodoDTO) {
		tipoMetodoDTO.setId(metodoDTO.getId());
		List<AppCacheDTO> lista = new ArrayList<AppCacheDTO>();
		appCacheDTO.setEstado("ACTIVO");
		appCacheDTO.setObjectIn(objectJsonIn);
		appCacheDTO.setObjectOut(objectJsonOut);
		appCacheDTO.setPrimer_validador(primerValidador);
		appCacheDTO.setSegundo_validador(segundoValidador);
		appCacheDTO.setDias(metodoDTO.getDias_cache());
		lista.add(appCacheDTO);
		tipoMetodoDTO.setCache(lista);
	}

}
