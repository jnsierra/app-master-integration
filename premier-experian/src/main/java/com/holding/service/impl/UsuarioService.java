package com.holding.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.holding.entity.UsuarioEntity;
import com.holding.repository.IUsuarioRepository;
import com.holding.service.IUsuarioService;



@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	IUsuarioRepository uRepository;


	@Override
	public String token(String code) {
		String token = uRepository.obtenerToken(code).getToken();
		
		return token;
	}


}
