package com.holding.service;

import java.util.List;

import com.parameter.dto.AplicacionIntegraDTO;
import com.parameter.dto.ConfigAdicionalDTO;

public interface IParameterService {

	final String url = "http://localhost:8083/api/admin/admin-rest/v1.0/aplicacionesInt/by/";
	
	//final String codigo = "Premier";
	
	AplicacionIntegraDTO parameters();
	
	String filtro(String clave,List<ConfigAdicionalDTO> param);
}
