package com.holding.service;

import org.springframework.http.HttpStatus;

import com.parameter.dto.AppCacheDTO;
import com.parameter.dto.TipoMetodoDTO;

public interface IAppCacheService {

	final String urlSaveCache = "http://localhost:8083/api/admin/admin-rest/v1.0/metodosInt/cacheInt/";
	final String urlExistsCache = "http://localhost:8083/api/admin/admin-rest/v1.0/cacheInt/";
	final String urlMethodByCode = "http://localhost:8083/api/admin/admin-rest/v1.0/metodosInt/by/codigo/";

	/**
	 * Metodo que se encarga de insertar en la base de datos la cache del servicio.
	 * 
	 * @param _consultarDatosLocalizacion_parameters
	 * @param objectJsonOut
	 * @return
	 */
	public HttpStatus insertCache(String primerValidador, String segundoValidador, String objectJsonIn,
			String objectJsonOut, TipoMetodoDTO metodoDTO);

	/**
	 * Metodo que se encarha de validar si ya existe o no una cache especifica en la
	 * base de datos.
	 * 
	 * @param consultarDatosLocalizacion
	 * @return
	 */
	public AppCacheDTO existsCache(String primerValidador, String segundoValidador, String metodo);

	/**
	 * Metodo que se encarga de consultar los datos de los metodos utilizando como
	 * filtro el codigo.
	 * 
	 * @param code
	 * @return
	 */
	public TipoMetodoDTO getMethodByCode(String code);

}
