package com.holding.abs;

import org.springframework.beans.factory.annotation.Autowired;

import com.holding.service.IUsuarioService;

public abstract class UtilAbs {
	@Autowired
	private IUsuarioService usuarioService;

	/**
	 * Metodo que se encarga de recuperar el token de la base de datos
	 * 
	 * @return
	 */
	public String getToken() {
		return usuarioService.token("001");
	}

}
