package com.holding.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.holding.entity.UsuarioEntity;

@Repository
public interface IUsuarioRepository extends JpaRepository<UsuarioEntity, Long>, CrudRepository<UsuarioEntity, Long> {
	/**
	 * Metodo con el cual obtengo todos los usuarios parametrizados en el sistema
	 * @return
	 */
	UsuarioEntity obtenerToken(@Param("codigo") String code);

	
}
