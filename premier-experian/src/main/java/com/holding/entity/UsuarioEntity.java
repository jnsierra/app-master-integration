package com.holding.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "app_usuarios", uniqueConstraints = {
		@UniqueConstraint(name = "UK_USUARIO_USUARIO", columnNames = { "usuario" }),
		@UniqueConstraint(name = "UK_USUARIO_CODIGO", columnNames = { "codigo" })
})
@NamedQueries({
		@NamedQuery(name = "UsuarioEntity.obtenerToken", query = " from UsuarioEntity u where u.codigo = :codigo")})
@Getter
@Setter
public class UsuarioEntity {
	
	@Id
	@Column(name = "id_us", nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_generator")
	@SequenceGenerator(name = "usuario_generator", sequenceName = "usuario_seq", allocationSize = 1)
	private Long id;
	@Column(name = "usuario")
	private String usuario;
	@Column(name = "codigo", nullable = false)
	private String codigo;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "token")
	private String token;

	public UsuarioEntity() {
		super();
	}

}