package co.premier.abs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public abstract class UtilAbs {

	/**
	 * Metodo que se encarga de sumas N cantidad de dias a la fecha actual.
	 * 
	 * @param days
	 * @return
	 */
	public Date calculateDate(Long days) {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DAY_OF_YEAR, days.intValue());
			return new SimpleDateFormat("yyyy-MM-dd")
					.parse(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
