package co.premier.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.premier.bussines.service.IAppCacheService;
import co.premier.repository.entity.AppCacheEntity;
import co.premier.utiles.dto.AppCacheDTO;
import co.premier.utiles.dto.respuesta.ResponseRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/v1.0/cacheInt")
@Api(value = "Rest Controller para manejar la cache.")
public class AppCacheRest {

	@Autowired
	IAppCacheService appCacheService;

	@Autowired
	ModelMapper mapper;

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Este servicio se encarga de consultar si existe o no una cache especifica en la tabla app_cache.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de consultar si existe o no una cache especifica en la tabla app_cache."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de consultar si existe o no una cache especifica en la tabla app_cache."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de consultar si existe o no una cache especifica en la tabla app_cache.") })
	public ResponseEntity<ResponseRestService<AppCacheDTO>> getCache(
			@RequestParam("primerValidador") String primerValidador,
			@RequestParam("segundoValidador") String segundoValidador,
			@RequestParam("nombreServicio") String nombreServicio) {
		AppCacheEntity apps = appCacheService.consultarCache(primerValidador, segundoValidador, nombreServicio);
		if (apps == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(new ResponseRestService<AppCacheDTO>(mapper.map(apps, AppCacheDTO.class)),
				HttpStatus.OK);
	}

}
