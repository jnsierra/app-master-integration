package co.premier.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.premier.abs.UtilAbs;
import co.premier.bussines.service.impl.ImplAppCacheService;
import co.premier.bussines.service.impl.ImplTipoModeloService;
import co.premier.repository.entity.AppCacheEntity;
import co.premier.repository.entity.TipoMetodoEntity;
import co.premier.utiles.dto.AppCacheDTO;
import co.premier.utiles.dto.TipoMetodoDTO;
import co.premier.utiles.dto.respuesta.ResponseRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/v1.0/metodosInt")
@Api(value = "Rest Controller para los Metodos.")
public class TipoMetodoRest extends UtilAbs {

	@Autowired
	ImplTipoModeloService tipoModeloService;

	@Autowired
	ImplAppCacheService appCacheService;

	@Autowired
	ModelMapper mapper;

	@RequestMapping(value = "/by/codigo/", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Este servicio se encargara de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el Codigo.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el Codigo."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el Codigo."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el Codigo.") })
	public ResponseEntity<ResponseRestService<TipoMetodoDTO>> findByCode(@RequestParam("codigo") String codigo) {
		TipoMetodoEntity tipo = tipoModeloService.consultarPorCodigo(codigo);
		if (tipo == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(new ResponseRestService<>(mapper.map(tipo, TipoMetodoDTO.class)), HttpStatus.OK);
	}

	@RequestMapping(value = "/by/", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Este servicio se encargara de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el id.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el id."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el id."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de consultar la informacion de la tabla app_tipo_metodo utilizando como filtro el id.") })
	public ResponseEntity<ResponseRestService<TipoMetodoDTO>> findById(@RequestParam("id") Long id) {
		TipoMetodoEntity tipo = tipoModeloService.get(id);
		if (tipo == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(new ResponseRestService<>(mapper.map(tipo, TipoMetodoDTO.class)), HttpStatus.OK);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Este servicio se encargara de consultar toda la informacion de la tabla app_tipo_metodo.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de consultar toda la informacion de la tabla app_tipo_metodo."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de consultar toda la informacion de la tabla app_tipo_metodo."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de consultar toda la informacion de la tabla app_tipo_metodo.") })
	public ResponseEntity<ResponseRestService<TipoMetodoDTO[]>> findAll() {
		List<TipoMetodoEntity> tipo = tipoModeloService.getAll();
		if (tipo == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(new ResponseRestService<TipoMetodoDTO[]>(mapper.map(tipo, TipoMetodoDTO[].class)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/", method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "Este servicio se encargara de 'eliminar' una registro de la tabla app_tipo_metodo realizando el cambio de estado de ACTIVO por INACTIVO, utilizando como parametro el codigo.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de actualizar el estado de la tabla app_tipo_metodo."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de de actualizar el estado de la tabla app_tipo_metodo."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de actualizar el estado de la tabla app_tipo_metodo.") })
	public ResponseEntity<ResponseRestService<Boolean>> deleteById(@RequestParam("id") Long id) {
		if (!tipoModeloService.delete(id)) {
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
		}
		return new ResponseEntity<>(new ResponseRestService<>(Boolean.TRUE), HttpStatus.OK);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Este servicio se encargara de guardar un registro en la tabla app_tipo_metodo.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de guardar un registro en la tabla app_tipo_metodo."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de guardar un registro en la tabla app_tipo_metodo."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de guardar un registro en la tabla app_tipo_metodo") })
	public ResponseEntity<ResponseRestService<TipoMetodoDTO>> saveEntity(
			@RequestBody(required = true) TipoMetodoDTO tipoDTO) {
		TipoMetodoDTO app = mapper.map(tipoModeloService.post(mapper.map(tipoDTO, TipoMetodoEntity.class)),
				TipoMetodoDTO.class);
		return new ResponseEntity<>(new ResponseRestService<>(app), HttpStatus.OK);
	}

	@RequestMapping(value = "/", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Este servicio se encargara de actualizar un registro en la tabla app_tipo_metodo.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de actualizar un registro en la tabla app_tipo_metodo."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de actualizar un registro en la tabla app_tipo_metodo."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de actualizar un registro en la tabla app_tipo_metodo") })
	public ResponseEntity<ResponseRestService<TipoMetodoDTO>> updateUsuario(
			@RequestBody(required = true) TipoMetodoDTO tipoDTO) {
		Optional<TipoMetodoEntity> usuarioT = tipoModeloService.put(mapper.map(tipoDTO, TipoMetodoEntity.class));
		if (!usuarioT.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
		}
		return new ResponseEntity<>(new ResponseRestService<>(tipoDTO), HttpStatus.OK);

	}

	@RequestMapping(value = "/cacheInt/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Este servicio se encargara de guardar la cache de los servicios en la tabla app_cache.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de guardar la cache de los servicios en la tabla app_cache."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de guardar la cache de los servicios en la tabla app_cache."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de guardar la cache de los servicios en la tabla app_cache.") })
	public ResponseEntity<ResponseRestService<List<AppCacheDTO>>> saveCache(
			@RequestBody(required = true) TipoMetodoDTO tipoMetodoDTO) {
		List<AppCacheDTO> lista = new ArrayList<AppCacheDTO>();

		for (AppCacheDTO item : tipoMetodoDTO.getCache()) {
			item.setEndDate(calculateDate(item.getDias()));
			AppCacheEntity appCacheEntity = mapper.map(item, AppCacheEntity.class);
			appCacheEntity.setMetodo(new TipoMetodoEntity());
			appCacheEntity.getMetodo().setId(tipoMetodoDTO.getId());
			AppCacheDTO appCacheDTO = mapper.map(appCacheService.insert(appCacheEntity), AppCacheDTO.class);
			lista.add(appCacheDTO);
		}
		return new ResponseEntity<>(new ResponseRestService<List<AppCacheDTO>>(lista), HttpStatus.OK);
	}

}
