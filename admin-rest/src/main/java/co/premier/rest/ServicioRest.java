package co.premier.rest;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.premier.bussines.service.IServicioAplicacionService;
import co.premier.bussines.service.impl.ImplTipoModeloService;
import co.premier.repository.entity.ServicioEntity;
import co.premier.repository.entity.TipoMetodoEntity;
import co.premier.utiles.dto.ServicioDTO;
import co.premier.utiles.dto.TipoMetodoDTO;
import co.premier.utiles.dto.respuesta.ResponseRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/v1.0/serviciosInt")
@Api(value = "Rest Controller para los Servicios.")
public class ServicioRest {

	@Autowired
	IServicioAplicacionService servicioAplicacionService;

	@Autowired
	ImplTipoModeloService tipoModeloService;

	@Autowired
	ModelMapper mapper;

	@RequestMapping(value = "/", method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "Este servicio se encargara de 'eliminar' una registro de la tabla app_servicio realizando el cambio de estado de ACTIVO por INACTIVO, utilizando como filtro el id.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de actualizar el estado  de la tabla app_servicio."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de de actualizar el estado de la tabla app_servicio."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de actualizar el estado de la tabla app_servicio.") })
	public ResponseEntity<ResponseRestService<Boolean>> deleteById(@RequestParam("id") Long id) {
		return new ResponseEntity<>(new ResponseRestService<Boolean>(servicioAplicacionService.deleteById(id)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/by/", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Este servicio se encargara de consultar la informacion de la tabla app_servicio utilizando como filtro el id.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de consultar la informacion de la tabla app_servicio."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de consultar la informacion de la tabla app_servicio."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de consultar la informacion de la tabla app_servicio.") })
	public ResponseEntity<ResponseRestService<ServicioDTO>> getById(@RequestParam("id") Long id) {
		Optional<ServicioEntity> service = servicioAplicacionService.getByID(id);

		if (!service.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(new ResponseRestService<ServicioDTO>(mapper.map(service.get(), ServicioDTO.class)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/metodosInt/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Este servicio se encargara de guardar los metodos de los servicios en la tabla app_tipo_metodo.")
	@ApiResponses(value = {
			@ApiResponse(code = 401, message = "No está autorizado para acceder al servicio que se encarga de guardar los metodos de los servicios en la tabla app_tipo_metodo."),
			@ApiResponse(code = 403, message = "No está autenticado para acceder al servicio que se encarga de guardar  los metodos de los servicios en la tabla app_tipo_metodo."),
			@ApiResponse(code = 404, message = "No encuentra el servicio que se encarga de guardar  los metodos de los servicios en la tabla app_tipo_metodo.") })
	public ResponseEntity<ResponseRestService<TipoMetodoDTO>> saveTypesMethods(
			@RequestBody(required = true) ServicioDTO servicioDto) {
		TipoMetodoEntity tipoMetodoEntity = mapper.map(servicioDto.getTipMetodo(), TipoMetodoEntity.class);
		tipoMetodoEntity.setServicio(new ServicioEntity());
		tipoMetodoEntity.getServicio().setId(servicioDto.getId());
		TipoMetodoDTO tipoMetodoDTO = mapper.map(tipoModeloService.post(tipoMetodoEntity), TipoMetodoDTO.class);
		return new ResponseEntity<ResponseRestService<TipoMetodoDTO>>(new ResponseRestService<>(tipoMetodoDTO),
				HttpStatus.OK);

	}

}
